# Fore
Fore is a completely free and open source image board client for Android with a
simple feature set and a modern user interface. Users can browse and search
boards, threads, and posts, and download and share images and videos.

## Compilation
Those seeking to compile must have...

- Node.js 16.2
- Android Studio 4.2
- Android SDK 30

Once these requirements have been met, simply clone the repository and refer to
the official Ionic documentation for further instructions on how to build.
